﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MastiAPI.Models;
namespace MastiAPI.Class
{
    public class SynchronizeData
    {
        public List<Animal> Animals;
        public List<Dose> Doses;
        public List<Farm> Farms;
        public List<Medicine> Medicines;
        public List<Milking> Milkings;
        public List<Treatment> Treatments;
        public List<Disease> Diseases;

        //public static List<DbSet<Animal>> ListAnimals;
        //public static List<DbSet<Dose>> ListDoses;
        //public static List<DbSet<Farm>> ListFarms;
        //public static List<DbSet<Medicine>> ListMedicines;
        //public static List<DbSet<Milking>> ListMilkings;
        //public static List<DbSet<Treatment>> ListTreatments;
        //public static List<DbSet<Disease>> ListDiseases;
        //public string FarmName;
        //public string FarmAddress;
        //public string FarmLongitude;
        //public string FarmLatitude;
        //public string AnimalID;
        //public int FarmID;
        //public string MedicineName;
        //public bool MedicineCatalog;
        //public int MedicineInterval;
        //public int MedicineNumberOfDoses;
        //public int MedicineDiscardDays;
        //public string MedicineSupplier;
        //public string MedicineAdministrationMethod;
        //public int TreatmentDiscardDays;
        //public string TreatmentStartDate;
        //public int TreatmentDosesTargetNumber;
        //public string TreatmentAffectedMammaryGlands;
        //public int TreatmentMastitsLevel;
        //public string TreatmentTimeOfDay;
        //public string TreatmentDate;
        //public string MilkingMedicineIDS;
        //public string MilkingTreatmentIDS;
        //public int MedicineFarmID;
        //public string MedicineCombines;
        //public string TreatmentCausetiveBacteria;
    }
}