﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MastiAPI.Models;

namespace MastiAPI.Controllers
{
    public class DosesController : ApiController
    {
        private MastiAPIContext db = new MastiAPIContext();

        // GET: api/Doses
        public IQueryable<Dose> GetDoses()
        {
            return db.Doses;
        }

        // GET: api/Doses/5
        [ResponseType(typeof(Dose))]
        public async Task<IHttpActionResult> GetDose(int id)
        {
            Dose dose = await db.Doses.FindAsync(id);
            if (dose == null)
            {
                return NotFound();
            }

            return Ok(dose);
        }

        // PUT: api/Doses/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDose(int id, Dose dose)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dose.Id)
            {
                return BadRequest();
            }

            db.Entry(dose).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DoseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Doses
        [ResponseType(typeof(Dose))]
        public async Task<IHttpActionResult> PostDose(Dose dose)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Doses.Add(dose);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = dose.Id }, dose);
        }

        // DELETE: api/Doses/5
        [ResponseType(typeof(Dose))]
        public async Task<IHttpActionResult> DeleteDose(int id)
        {
            Dose dose = await db.Doses.FindAsync(id);
            if (dose == null)
            {
                return NotFound();
            }

            db.Doses.Remove(dose);
            await db.SaveChangesAsync();

            return Ok(dose);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DoseExists(int id)
        {
            return db.Doses.Count(e => e.Id == id) > 0;
        }
    }
}