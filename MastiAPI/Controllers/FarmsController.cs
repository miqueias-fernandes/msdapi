﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MastiAPI.Models;

namespace MastiAPI.Controllers
{
    public class FarmsController : ApiController
    {
        private MastiAPIContext db = new MastiAPIContext();

        // GET: api/Farms
        public IQueryable<Farm> GetFarms()
        {
            return db.Farms;
        }

        // GET: api/Farms/5
        [ResponseType(typeof(Farm))]
        public async Task<IHttpActionResult> GetFarm(int id)
        {
            Farm farm = await db.Farms.FindAsync(id);
            if (farm == null)
            {
                return NotFound();
            }

            return Ok(farm);
        }

        // PUT: api/Farms/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFarm(int id, Farm farm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farm.Id)
            {
                return BadRequest();
            }

            db.Entry(farm).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FarmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Farms
        [ResponseType(typeof(Farm))]
        public async Task<IHttpActionResult> PostFarm(Farm farm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Farms.Add(farm);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = farm.Id }, farm);
        }

        // DELETE: api/Farms/5
        [ResponseType(typeof(Farm))]
        public async Task<IHttpActionResult> DeleteFarm(int id)
        {
            Farm farm = await db.Farms.FindAsync(id);
            if (farm == null)
            {
                return NotFound();
            }

            db.Farms.Remove(farm);
            await db.SaveChangesAsync();

            return Ok(farm);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FarmExists(int id)
        {
            return db.Farms.Count(e => e.Id == id) > 0;
        }
    }
}