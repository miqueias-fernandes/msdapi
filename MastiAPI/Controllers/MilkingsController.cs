﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MastiAPI.Models;

namespace MastiAPI.Controllers
{
    public class MilkingsController : ApiController
    {
        private MastiAPIContext db = new MastiAPIContext();

        // GET: api/Milkings
        public IQueryable<Milking> GetMilkings()
        {
            return db.Milkings;
        }

        // GET: api/Milkings/5
        [ResponseType(typeof(Milking))]
        public async Task<IHttpActionResult> GetMilking(int id)
        {
            Milking milking = await db.Milkings.FindAsync(id);
            if (milking == null)
            {
                return NotFound();
            }

            return Ok(milking);
        }

        // PUT: api/Milkings/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMilking(int id, Milking milking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != milking.Id)
            {
                return BadRequest();
            }

            db.Entry(milking).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MilkingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Milkings
        [ResponseType(typeof(Milking))]
        public async Task<IHttpActionResult> PostMilking(Milking milking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Milkings.Add(milking);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = milking.Id }, milking);
        }

        // DELETE: api/Milkings/5
        [ResponseType(typeof(Milking))]
        public async Task<IHttpActionResult> DeleteMilking(int id)
        {
            Milking milking = await db.Milkings.FindAsync(id);
            if (milking == null)
            {
                return NotFound();
            }

            db.Milkings.Remove(milking);
            await db.SaveChangesAsync();

            return Ok(milking);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MilkingExists(int id)
        {
            return db.Milkings.Count(e => e.Id == id) > 0;
        }
    }
}