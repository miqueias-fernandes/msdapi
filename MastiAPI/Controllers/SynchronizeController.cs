﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MastiAPI.Models;
using MastiAPI.Class;
using System.Data.Entity;
namespace MastiAPI.Controllers
{
    public class SynchronizeController : ApiController
    {
        private MastiAPIContext db = new MastiAPIContext();

        // GET: api/Synchronize/5
        //[ResponseType(typeof(Animal))]
        [Route("api/Synchronize/{date}/{time}")]
        public IEnumerable<List<Model>> Get([FromUri]string date,[FromUri]string time)
        {
            //Parse URI string to DateTime
            DateTime UriDateTime = DateTime.Parse(date.Replace("-", "/") + " " + time.Replace("-", ":"));

            //Put in list returned data of other Controllers
            AnimalsController animals = new AnimalsController();
            DiseasesController diseases = new DiseasesController();
            DosesController doses = new DosesController();
            FarmsController farms = new FarmsController();
            MedicinesController medicines = new MedicinesController();
            MilkingsController milkings = new MilkingsController();
            TreatmentsController treatments = new TreatmentsController();

            //
            List<IQueryable<Model>> syncData = new List<IQueryable<Model>>();

            syncData.Add(animals.GetAnimals());
            syncData.Add(diseases.GetDiseases());
            syncData.Add(doses.GetDoses());
            syncData.Add(farms.GetFarms());
            syncData.Add(medicines.GetMedicines());
            syncData.Add(milkings.GetMilkings());
            syncData.Add(treatments.GetTreatments());
            
            //Create a result List to receive parsed data
            List<List<Model>> result = new List<List<Model>>();

            if (UriDateTime != null)
            {
                foreach (IQueryable<Model> subList in syncData)
                {
                    List<Model> itensList = new List<Model>();

                    foreach (Model record in subList)
                    {
                        if (record.UpdatedAt > UriDateTime)
                        {
                            itensList.Add(record);
                        }
                    }
                    result.Add(itensList);

                }
            }

            foreach (List<Model> subList in result.ToList())
            {
                if (subList.Count == 0)
                {
                    result.Remove(subList);
                }

                if (result.Count == 0)
                {
                    break;
                }
            }

            return result;
        }

        // POST: api/Synchronize
        //[ResponseType(typeof(Animal))]
        public async Task<IHttpActionResult> Post(SynchronizeData data)
        {
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            foreach (Milking milking in data.Milkings)
            {
                milking.CreatedAt = DateTime.Now;
                milking.UpdatedAt = DateTime.Now;
                db.Milkings.Add(milking);
            }
            await db.SaveChangesAsync();

            foreach (Farm farm in data.Farms)
            {
                farm.CreatedAt = DateTime.Now;
                farm.UpdatedAt = DateTime.Now;
                db.Farms.Add(farm);
            }
            await db.SaveChangesAsync();

            foreach (Animal animal in data.Animals)
            {
                animal.CreatedAt = DateTime.Now;
                animal.UpdatedAt = DateTime.Now;
                db.Animals.Add(animal);
            }
            await db.SaveChangesAsync();

            foreach (Medicine medicine in data.Medicines)
            {
                medicine.CreatedAt = DateTime.Now;
                medicine.UpdatedAt = DateTime.Now;
                db.Medicines.Add(medicine);
            }
            await db.SaveChangesAsync();

            foreach (Disease disease in data.Diseases)
            {
                disease.CreatedAt = DateTime.Now;
                disease.UpdatedAt = DateTime.Now;
                db.Diseases.Add(disease);
            }
            await db.SaveChangesAsync();

            foreach (Treatment treatment in data.Treatments)
            {
                treatment.CreatedAt = DateTime.Now;
                treatment.UpdatedAt = DateTime.Now;
                db.Treatments.Add(treatment);
            }

            await db.SaveChangesAsync();

            foreach (Dose dose in data.Doses)
            {
                dose.CreatedAt = DateTime.Now;
                dose.UpdatedAt = DateTime.Now;
                db.Doses.Add(dose);
            }
            await db.SaveChangesAsync();

            return Ok(data);
        }

        }

    }

