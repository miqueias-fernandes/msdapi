﻿namespace MastiAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Animals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Identifier = c.String(nullable: false),
                        FarmID = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Farms", t => t.FarmID, cascadeDelete: true)
                .Index(t => t.FarmID);
            
            CreateTable(
                "dbo.Doses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Number = c.Int(nullable: false),
                        AnimalID = c.Int(nullable: false),
                        TreatmentID = c.Int(nullable: false),
                        MedicineID = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Animals", t => t.AnimalID, cascadeDelete: true)
                .ForeignKey("dbo.Medicines", t => t.MedicineID, cascadeDelete: true)
                .ForeignKey("dbo.Tratments", t => t.TreatmentID, cascadeDelete: true)
                .Index(t => t.AnimalID)
                .Index(t => t.TreatmentID)
                .Index(t => t.MedicineID);
            
            CreateTable(
                "dbo.Medicines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Interval = c.Int(nullable: false),
                        NumberOfDoses = c.Int(nullable: false),
                        DiscardDays = c.Int(nullable: false),
                        Supplier = c.String(nullable: false),
                        AdministrationMethod = c.String(nullable: false),
                        Catalog = c.Boolean(nullable: false),
                        PhotoURL = c.String(),
                        FarmID = c.Int(nullable: false),
                        CombinesWith = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Farms", t => t.FarmID, cascadeDelete: false)
                .Index(t => t.FarmID);
            
            CreateTable(
                "dbo.Farms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        Longitude = c.String(nullable: false),
                        Latitude = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tratments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        DosesTargetNumber = c.Int(nullable: false),
                        AffectedMammaryGlands = c.String(nullable: false),
                        MastitsLevel = c.Int(nullable: false),
                        CausativeBacteria = c.String(),
                        DiscardDays = c.Int(nullable: false),
                        DiscardState = c.Boolean(nullable: false),
                        DiseaseID = c.Int(nullable: false),
                        AnimalID = c.Int(nullable: false),
                        EndCarency = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Animals", t => t.AnimalID, cascadeDelete: false)
                .ForeignKey("dbo.Diseases", t => t.DiseaseID, cascadeDelete: true)
                .Index(t => t.DiseaseID)
                .Index(t => t.AnimalID);
            
            CreateTable(
                "dbo.Diseases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Milkings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeOfDay = c.String(nullable: false),
                        TreatmentIds = c.String(),
                        MedicineIds = c.String(),
                        Date = c.DateTime(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Animals", "FarmID", "dbo.Farms");
            DropForeignKey("dbo.Doses", "TreatmentID", "dbo.Tratments");
            DropForeignKey("dbo.Tratments", "DiseaseID", "dbo.Diseases");
            DropForeignKey("dbo.Tratments", "AnimalID", "dbo.Animals");
            DropForeignKey("dbo.Medicines", "FarmID", "dbo.Farms");
            DropForeignKey("dbo.Doses", "MedicineID", "dbo.Medicines");
            DropForeignKey("dbo.Doses", "AnimalID", "dbo.Animals");
            DropIndex("dbo.Tratments", new[] { "AnimalID" });
            DropIndex("dbo.Tratments", new[] { "DiseaseID" });
            DropIndex("dbo.Medicines", new[] { "FarmID" });
            DropIndex("dbo.Doses", new[] { "MedicineID" });
            DropIndex("dbo.Doses", new[] { "TreatmentID" });
            DropIndex("dbo.Doses", new[] { "AnimalID" });
            DropIndex("dbo.Animals", new[] { "FarmID" });
            DropTable("dbo.Milkings");
            DropTable("dbo.Diseases");
            DropTable("dbo.Tratments");
            DropTable("dbo.Farms");
            DropTable("dbo.Medicines");
            DropTable("dbo.Doses");
            DropTable("dbo.Animals");
        }
    }
}
