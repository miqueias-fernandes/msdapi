﻿namespace MastiAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Optional_Medicine_Farm_Id : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Medicines", "FarmID", "dbo.Farms");
            DropIndex("dbo.Medicines", new[] { "FarmID" });
            AlterColumn("dbo.Medicines", "FarmID", c => c.Int());
            CreateIndex("dbo.Medicines", "FarmID");
            AddForeignKey("dbo.Medicines", "FarmID", "dbo.Farms", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Medicines", "FarmID", "dbo.Farms");
            DropIndex("dbo.Medicines", new[] { "FarmID" });
            AlterColumn("dbo.Medicines", "FarmID", c => c.Int(nullable: false));
            CreateIndex("dbo.Medicines", "FarmID");
            AddForeignKey("dbo.Medicines", "FarmID", "dbo.Farms", "Id", cascadeDelete: true);
        }
    }
}
