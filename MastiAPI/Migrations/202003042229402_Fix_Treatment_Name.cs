﻿namespace MastiAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fix_Treatment_Name : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Tratments", newName: "Treatments");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Treatments", newName: "Tratments");
        }
    }
}
