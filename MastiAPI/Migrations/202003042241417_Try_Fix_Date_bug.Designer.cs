﻿// <auto-generated />
namespace MastiAPI.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class Try_Fix_Date_bug : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Try_Fix_Date_bug));
        
        string IMigrationMetadata.Id
        {
            get { return "202003042241417_Try_Fix_Date_bug"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
