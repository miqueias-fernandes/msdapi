﻿namespace MastiAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Try_Fix_Date_bug : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Treatments", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Treatments", "EndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Treatments", "EndDate");
            DropColumn("dbo.Treatments", "StartDate");
        }
    }
}
