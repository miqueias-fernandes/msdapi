﻿namespace MastiAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fix_Date_bug : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Treatments", "EndCarency", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Treatments", "EndCarency", c => c.DateTime(nullable: false));
        }
    }
}
