﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MastiAPI.Models
{
    [Table("Animals")]
    public class Animal:Model
    {
        [Required]
        public string Identifier { get; set; }
        [Required]
        public int FarmID { get; set; }
        public Farm Farm { get; set; }
        public ICollection<Treatment> Treatments { get; set; }
        public ICollection<Dose> Doses { get; set; }
    }
}