﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MastiAPI.Models
{
    [Table("Diseases")]
    public class Disease:Model
    {
        [Required]
        public string Name { get; set; }
        public ICollection<Treatment> Treatments { get; set; }
    }
}