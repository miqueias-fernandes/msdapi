﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MastiAPI.Models
{
    [Table("Doses")]
    public class Dose:Model
    {
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        public int AnimalID { get; set; }
        public Animal Animal { get; set; }
        [Required]
        public int TreatmentID { get; set; }
        public Treatment Treatment { get; set; }
        [Required]
        public int MedicineID { get; set; }
        public Medicine Medicine { get; set; }

    }
}