﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MastiAPI.Models
{
    public class MastiAPIContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public MastiAPIContext() : base("name=MastiAPIContext")
        {
        }

        public System.Data.Entity.DbSet<MastiAPI.Models.Milking> Milkings { get; set; }

        public System.Data.Entity.DbSet<MastiAPI.Models.Farm> Farms { get; set; }

        public System.Data.Entity.DbSet<MastiAPI.Models.Animal> Animals { get; set; }

        public System.Data.Entity.DbSet<MastiAPI.Models.Disease> Diseases { get; set; }

        public System.Data.Entity.DbSet<MastiAPI.Models.Dose> Doses { get; set; }

        public System.Data.Entity.DbSet<MastiAPI.Models.Medicine> Medicines { get; set; }

        public System.Data.Entity.DbSet<MastiAPI.Models.Treatment> Treatments { get; set; }
    }
}
