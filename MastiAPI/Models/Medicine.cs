﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MastiAPI.Models
{
    [Table("Medicines")]
    public class Medicine:Model
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Interval { get; set; }
        [Required]
        public int NumberOfDoses { get; set; }
        [Required]
        public int DiscardDays { get; set; }
        [Required]
        public string Supplier { get; set; }
        [Required]
        public string AdministrationMethod { get; set; }
        [Required]
        public bool Catalog { get; set; }
        public string PhotoURL { get; set; }
        public int? FarmID { get; set; }
        public Farm Farm { get; set; }
        //[ForeignKey("CombinesWith")]
        public int CombinesWith {get;set;}
        //public Medicine Combine { get; set; }
        public ICollection<Dose> Doses { get; set; }
    }
}