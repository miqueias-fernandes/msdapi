﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace MastiAPI.Models
{
    [Table("Milkings")]
    public class Milking:Model
    {
        [Required]
        public string TimeOfDay { get; set; }
        public string TreatmentIds {get;set;}
        public string MedicineIds { get; set; }
        [Required]
        public DateTime Date { get; set; }
    }
}