﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MastiAPI.Models
{
    [Table("Treatments")]
    public class Treatment : Model
    {
        [Required]
        public DateTime StartDate { get; set;}
        public DateTime? EndDate { get; set;}
        [Required]
        public int DosesTargetNumber { get; set; }
        [Required]
        public string AffectedMammaryGlands { get; set; }
        [Required]
        public int MastitsLevel { get; set; }
        public string CausativeBacteria { get; set; }
        [Required]
        public int DiscardDays { get; set; }
        [Required]
        public bool DiscardState { get; set; }
        [Required]
        public int DiseaseID { get; set; }
        public Disease Disease { get; set; }
        [Required]
        public int AnimalID { get; set; }
        public Animal Animal { get; set; }
        public DateTime? EndCarency { get; set; }
        public ICollection<Dose> Doses { get; set; }

    }
}