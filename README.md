# Web API - MSD (Milking Control)
	EC2 needs an elastic ip and a port configured in the security groups so that the application can be used externally.
	1. In the project folder, open the file "MsdAPI.sln" with Visual Studio 2017;
	2. Right click on the "MsdAPI" project on the right side of the "Solution Explorer" panel and click on the "Publish" option.
	In the publication panel, select "Folder" >" Browse ..." to select the folder where the publication will be made 
	(Ex C:\Users\Administrator\Desktop\MsdAPI). After that, click on "Publish";
	3. Now, open IIS Manager. On the Dashboard, expand your connection information in the "Connection" panel, then right-click on the "Sites" option and select "Add Website ...";
	4. In the "Add Website" panel, enter a name for the application in Site Name, the .Net Framework Version of the API 
	(4.5 in this case). Select the folder we previously published with the name "MsdAPI" in Physical Path;
	5. Click "Connect as .."> "Specific User" and enter your user's credentials, and click "Ok";
	6. Finally, in "Ip Address" select your local IP, and in "Port" the port you have configured for external access in EC2 Security Groups and click "Ok".
	Important: Make sure that there is no other service running on the same port
	You can now access the API with the address:
	http: // [EC2 DNS]: [Port]
_________________________________________________________________________________________________________________________________________________________________________________________________

## Usage:

### Animals

#### GET http://[host]/api/Animals/?id=[id]
	Return Animals by Fardm ID
	
	Parameters:
		- id (integer)

#### POST http://[host]/api/Animals
	Create new Animal, treatment and miling plan

	Parameters:
		- id (string)
		- idFarm (integer)
_________________________________________________________________________________________________________________________________________________________________________________________________

### Farm

#### GET http://[host]/api/Farm/?id=[id]
	Return Farm by ID
	
	Parameters:
		- id (integer)
		
#### GET http://[host]/api/Farm
	Return all Farms

#### POST http://[host]/api/Farm
	Create new Farm

	Parameters:
		- name (string)
		- address (string)
		- latitude(string)
		- longitude(string)
		
#### PUT http://[host]/api/Farm
	Update a Farm columns by especified id

	Parameters:
		- id (integer)
		- name (string)
		- address (string)
		- latitude(string)
		- longitude(string)
_________________________________________________________________________________________________________________________________________________________________________________________________

### Medicine

#### GET http://[host]/api/Medicine/?id=[id]
	Return Medicine by ID
	
	Parameters:
		- id (integer)

#### GET http://[host]/api/Medicine
	Return all Medicines

#### POST http://[host]/api/Medicine
	Create new Farm

	Parameters:
		- name (string)
		- interval (integer)
		- numberOfDoses(integer)
		- discardDays(integer)
		- supplier(string)
		- administrationMethod(string)
		- combines (integer)
_________________________________________________________________________________________________________________________________________________________________________________________________

### Treatment

#### POST http://[host]/api/Treatment
	Create new Treatment

	Parameters:
		- animalId(string)
		- mastitsLevel(integer)
		- affectedMammaryGlands(string) - (Obs: select between front-left, front-right, back-left and back-right)
		- treatmentStartDate(integer) (Obs: yyyy-mm-dd )
		- causetiveBacteria(string) (Obs: Optional)

#### GET http://[host]/api/Treatment
	Return all Treatments

#### GET http://[host]/api/Treatment/?id=[id]
	Return Treatment by ID
	
	Parameters:
		- id (integer)

(Results are returned in JSON format.)